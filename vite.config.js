import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { visualizer } from 'rollup-plugin-visualizer'
import importToCDN from 'vite-plugin-cdn-import'
import viteCompression from 'vite-plugin-compression'

const path = require("path")
export default defineConfig({
    plugins: [
        vue(),
        // 打包分析
        visualizer({
            // 注意这里要设置为true，否则无效
            open: false,
            gzipSize: true,
            brotliSize: true
        }),
        // 压缩
        viteCompression({
            // 是否在控制台输出
            verbose: true,
            // 是否禁用
            disable: false,
            // 大于此值才会压缩, 单位b
            threshold: 10240,
            // 压缩算法
            algorithm: 'gzip',
            // 压缩后是否删除源文件
            deleteOriginFile: false,
        }),
        // CDN
        importToCDN({
            modules: [
                {
                    name: 'vue',
                    var: 'Vue',
                    // path: 'https://unpkg.com/vue@3.3.4/dist/vue.global.prod.js',
                    // path: 'https://cdn.bootcdn.net/ajax/libs/vue/3.3.4/vue.global.prod.min.js'
                    path: 'https://fastly.jsdelivr.net/npm/vue@3.4.15/dist/vue.global.min.js'
                },
                {
                    name: 'vue-demi',
                    var: 'VueDemi',
                    path: 'https://fastly.jsdelivr.net/npm/vue-demi@0.14.6/lib/index.iife.min.js',
                },
                {
                    name: 'vue-router',
                    var: 'VueRouter',
                    // path: 'https://cdn.bootcdn.net/ajax/libs/vue-router/4.2.1/vue-router.global.js',
                    path: 'https://fastly.jsdelivr.net/npm/vue-router@4.2.5/dist/vue-router.global.min.js',
                },
                {
                    name: 'axios',
                    var: 'axios',
                    path: 'https://fastly.jsdelivr.net/npm/axios@1.6.5/dist/axios.min.js',
                },
                {
                    name: 'html2canvas',
                    var: 'html2canvas',
                    path: 'https://fastly.jsdelivr.net/npm/html2canvas@1.4.1/dist/html2canvas.min.js',
                },
                {
                    name: 'ant-design-vue',
                    var: 'antd',
                    path: [
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/dayjs.min.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/customParseFormat.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/weekday.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/localeData.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/weekOfYear.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/weekYear.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/advancedFormat.js',
                        'https://fastly.jsdelivr.net/npm/dayjs@1.11.10/plugin/quarterOfYear.js',
                        'https://fastly.jsdelivr.net/npm/ant-design-vue@4.1.1/dist/antd.min.js',
                    ],
                    css: 'https://fastly.jsdelivr.net/npm/ant-design-vue@4.1.1/dist/reset.css',
                },
                {
                    name: '@wangeditor/editor',
                    var: 'wangEditor',
                    path: 'https://fastly.jsdelivr.net/npm/@wangeditor/editor@5.1.23/dist/index.min.js',
                    css: 'https://fastly.jsdelivr.net/npm/@wangeditor/editor@5.1.23/dist/css/style.css',
                    // path: 'https://unpkg.com/@wangeditor/editor@5.1.23/dist/index.js',
                    // css: 'https://unpkg.com/@wangeditor/editor@5.1.23/dist/css/style.css',
                },
            ],
        }),
    ],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
        extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    },
    // 开发或生产环境服务的公共基础路径
    base: './',
    // 开发环境
    server: {
        host: 'localhost',
        // 端口号
        port: 9527,
        // 是否开启HTTPS
        https: false,
        // 是否自动打开浏览器
        open: false,
        // 是否开启热更新
        hmr: true,
    },
    // 打包配置
    build: {
        target: 'modules',
        // 输出路径
        outDir: 'dist',
        // 静态资源路径
        assetsDir: 'static',
        // 混淆器
        minify: 'terser',
        // 是否生成 source map 文件
        sourcemap: false,
        // 页面
        rollupOptions: {
            index: path.resolve(__dirname, 'index.html'),
            // 文件名称
            output: {
                // [name].
                // 模块
                chunkFilename: `static/js/[hash].js`,
                // 入口
                entryFileNames: `static/js/[hash].js`,
                // 资产
                assetFileNames: `static/[ext]/[hash].[ext]`
            }
        },
        // 构建时是否清空
        emptyOutDir: true,
        // 其他配置
        terserOptions: {
            compress: {
                drop_debugger: true,
                drop_console: true,
            }
        },
    },
    // 依赖优化
    optimizeDeps: {
        // include: [],
    },
})