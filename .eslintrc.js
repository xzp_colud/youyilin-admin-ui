module.exports = {
    root: true,
    env: {
        browser: true, // browser global variables
        // es2021: true, // adds all ECMAScript 2021 globals and automatically sets the ecmaVersion parser option to 12.
        es2022: true,
    },
    parserOptions: {
        ecmaVersion: 12,
    },
    extends: [
        'plugin:vue/vue3-recommended'
    ],
    rules: {
        'no-debugger': import.meta.env.VITE_USER_NODE_ENV === 'production' ? 'warn' : 'off',
        'no-console': import.meta.env.VITE_USER_NODE_ENV === 'production' ? 'warn' : 'off',
        'vue/no-unused-vars': 'off',
        'vue/multi-word-component-names': 'off',
        'vue/v-on-event-hyphenation': 'off',
        'vue/max-attributes-per-line': 'off',
        'vue/valid-attribute-name': 'off',
        'vue/html-indent': 'off',
        'vue/attributes-order': 'off',
        'vue/html-closing-bracket-newline': 'off',
        'vue/multiline-html-element-content-newline': 'off',
        'vue/html-closing-bracket-spacing': 'off',
        'vue/attribute-hyphenation': 'off',
        'vue/singleline-html-element-content-newline': 'off',
        'vue/html-quotes': 'off',
    }
    // "root": true,
    // "env": {
    //   "node": true
    // },
    // "extends": [
    //   "plugin:vue/vue3-essential",
    //   "eslint:recommended"
    // ],
    // "parserOptions": {
    //   "parser": "@babel/eslint-parser"
    // },
    // "rules": {
    //   "no-debugger": "off",
    //   "no-console": "off",
    //   "vue/multi-word-component-names": "off"
    // }
}