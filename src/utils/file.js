// base64转换为file
export const dataURLtoFile = (dataurl, filename) => {
    const arr = dataurl.split(',')
    const mime = arr[0].match(/:(.*?);/)[1]
    const bstr = atob(arr[1])
    let n = bstr.length
    let u8arr = new Uint8Array(n)
    while(n--) {
        u8arr[n] = bstr.charCodeAt(n)
    }
    //转换成file对象
    return new File([u8arr], filename, { type: mime })
    //转换成成blob对象
    //return new Blob([u8arr],{type:mime});
}

// base64转换为blob
export const dataURLtoBlob = (dataurl) => {
    const arr = dataurl.split(',')
    const mime = arr[0].match(/:(.*?);/)[1]
    const bstr = atob(arr[1])
    let n = bstr.length
    let u8arr = new Uint8Array(n)
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n)
    }
    return new Blob([u8arr], { type: mime })
}

// blob转换为file
export const blobToFile = (theBlob, fileName) => {
    theBlob.lastModifiedDate = new Date()  // 文件最后的修改日期
    theBlob.name = fileName               // 文件名
    return new File([theBlob], fileName, { type: theBlob.type, lastModified: Date.now() })
}