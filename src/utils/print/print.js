import { getLodop } from '@/utils/cLodop/LodopFuncs'

const getPrintHtml = _bodyHtml => `<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
	${_bodyHtml}
</body>
</html>`

/**
 * 执行打印
 * @param {*} _html HTML
 * @param {*} _way 打印方向
 * @param {*} _width 宽度
 * @param {*} _height 高度
 */
export const doPrint = (_html, _way, _width, _height) => {
    const way = _way === 2 ? 2 : 1
    const width = _width ? _width : 2100
    const height = _height ? _height : 2970
    LODOP = getLodop()      
    LODOP.PRINT_INIT('打印表格')
    // 1 横向 2 纵向
    LODOP.SET_PRINT_PAGESIZE(way, width, height, '')
    LODOP.ADD_PRINT_HTM(0, 0, '100%', '100%', getPrintHtml(_html))
    LODOP.SET_PRINT_MODE('FULL_WIDTH_FOR_OVERFLOW', true)
    LODOP.SET_PRINT_MODE('FULL_HEIGHT_FOR_OVERFLOW', true)
    LODOP.SET_PRINT_MODE('CATCH_PRINT_STATUS', true)
    LODOP.PREVIEW()
}