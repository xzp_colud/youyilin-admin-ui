
import dayjs from 'dayjs'

const time = {
    year: (val) => {
        if (!val) return ''
        return dayjs(val).format('YYYY')
    },
    month: (val) => {
        if (!val) return ''
        return dayjs(val).format('YYYY-MM')
    },
    day: (val) => {
        if (!val) return ''
        return dayjs(val).format('YYYY-MM-DD')
    },
    hour: (val) => {
        if (!val) return ''
        return dayjs(val).format('YYYY-MM-DD HH')
    },
    min: (val) => {
        if (!val) return ''
        return dayjs(val).format('YYYY-MM-DD HH:mm')
    },
    second: (val) => {
        if (!val) return ''
        return dayjs(val).format('YYYY-MM-DD HH:mm:ss')
    },
    format: (val, format) => {
        if (!val || !format) return ''
        return dayjs(val).format(format)
    },
}
export default time