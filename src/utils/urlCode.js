
const code = {
    // 编码
    en: (val) => {
        return encodeURIComponent(JSON.stringify(val))
    },
    // 解码
    de: (val) => {
        try {
            const params = JSON.parse(decodeURIComponent(val))
            return params
        } catch (err) {
            return ''
        }
    },
}

export default code