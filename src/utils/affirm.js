import { createVNode } from 'vue'
import { ExclamationCircleOutlined } from '@/layouts/IconExample'
import { Modal } from 'ant-design-vue'

/**
 * 重写 封装 confirm 全局使用
 * @param {*} title    标题
 * @param {*} content  内容
 * @param {*} ok       方法
 */
const affirm = (title, content , ok) => {
    Modal.confirm({
        title: title,
        content: content,
        icon: createVNode(ExclamationCircleOutlined),
        onOk: ok,
        okText: '确定',
    })
}

export default affirm