import axios from 'axios'
// import qs from 'qs'
import { message, Modal } from 'ant-design-vue'

import router from '@/router/router'

import { ACCESS_TOKEN } from '@/store/constant'
import useStore from '@/store'

// 当前请求数量
var axiosNum = 0
// 请求头部
const headersContentType = 'Content-Type'
const headersContentTypeValue = 'application/json; charset=utf-8'
const headersContentTypeValueForm = 'application/x-www-form-urlencoded; charset=utf-8'
const headersToken = 'authorization'
const headersTimestamp = 'timestamp'

let instance = axios.create({
    baseURL: import.meta.env.VITE_APP_BASE_URL,
    headers: {
        'Access-Control-Allow-Credentials': 'true',
        'X-Requested-With': 'XMLHttpRequest',
    },
    withCredentials: true,
    timeout: 30000,
})

// 请求拦截器
instance.interceptors.request.use(config => {
    const { commonStore } = useStore()
    // 当前请求地址
    const url = config.url
    // 当前请求类型
    const method = config.method.toUpperCase()

    // POST 全局加载遮罩
    if(method === 'POST' || method === 'PUT'){
        commonStore.loading = config.loading === false ? false : true
    }

    // GET 反序列化请求参数
    if(method === 'GET'){
        // config.paramsSerializer = function(params) {
        //     return qs.stringify(params, { arrayFormat: 'repeat' })
        // }
        // 表格请求状态
        if (url.lastIndexOf('getPageList') >= 0) {
            commonStore.loadingTable = true
        }
    }

    // 携带的内容类型
    if (!config.headers[headersContentType]) {
        const form = config.form
        if (form === true) {
            config.headers[headersContentType] = headersContentTypeValueForm
        } else {
            config.headers[headersContentType] = headersContentTypeValue
        }
    }

    // 携带TOKEN
    const token = localStorage.getItem(ACCESS_TOKEN)
    if (token) {
        config.headers[headersToken] = token
    }

    // 携带当前时间
    config.headers[headersTimestamp] = new Date().getTime()

    return config
}, error => {
    return Promise.reject(error)
})

// 响应拦截器
instance.interceptors.response.use(response => {
    // 关闭相关遮罩
    closeLoading()

    // 刷新 Token
    const authorization = response.headers[headersToken]
    if (authorization) {
        localStorage.setItem(ACCESS_TOKEN, authorization)
    }

    // 提示消息
    const data = response.data
    if (data && data.msg) {
        if (data.success) {
            message.success(data.msg)
        } else {
            message.error(data.msg)
        }
    }

    if (data.success === true) {
        return response.data
    }

    return Promise.reject(data.msg)
}, error => {
    // 关闭相关遮罩
    closeLoading()

    // 重复提交
    if(error && error.response && error.response.status == 400) {
        if (error.response.data.msg != 'REPEAT') {
            // 重复提交表单的不做提示
            message.error(error.response.data.msg)
        }
    }

    // 未登录
    if(error && error.response && error.response.status == 401) {
        alertNoLogin()
    }

    // 没有权限
    if(error && error.response && error.response.status == 403) {
        message.error('禁止访问')
    }

    // 404
    if(error && error.response && error.response.status == 404) {
        // router.push('/404')
    }

    // 500
    if(error && error.response && error.response.status == 500) {
        // router.push('/500')
        message.error(error.response.data.msg)
    }

    return Promise.reject(error)
})

// 未登录提示标记
let noLoginAlterFlag = false
async function alertNoLogin() {
    if (noLoginAlterFlag) return
    noLoginAlterFlag = true
    localStorage.removeItem(ACCESS_TOKEN)
    Modal.destroyAll()
    Modal.confirm({
        title: '登录过期提醒！',
        content: '当前登录已过期，请重新登录。',
        keyboard: false,
        cancelButtonProps: {
            style: {
                display: 'none',
            },
        },
        okText: '确认',
        okButtonProps: {
            block: true,
        },
        onOk: () => router.replace({ path: '/login' })
    })
}

// 关闭相关遮罩
function closeLoading () {
    if (axiosNum > 0) {
        axiosNum = axiosNum - 1
    }
    // 关闭全局遮罩
    if (axiosNum == 0) {
        const { commonStore } = useStore()
        commonStore.loading = false
        commonStore.loadingTable = false
    }
}

export default instance