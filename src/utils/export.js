/**
 * 导出数据
 * @param {*} header 表头数组
 * @param {*} dataArray 导出数据
 * @param {*} fileName 文件名称
 */
export const domExportExcel = (header, dataArray, fileName) => {
    console.log(dataArray, 'dataArray')
    let dataString = ''
    for (let i = 0; i < header.length; i++) {
      dataString += `${header[i] + '\t'}` // 获取到表头后加上换行符进行拼接
    }
    dataString += '\n'
    for (let i = 0; i < dataArray.length; i++) {
      for (const key in dataArray[i]) {
        dataString += `${dataArray[i][key] + '\t'}` // 获取到每一行数据后加上换行符进行拼接
      }
      dataString += '\n'
    }
    const blob = new Blob([dataString], { type: 'application/vnd.ms-excel' })
    const time = Date.now() // 给表格名加个时间戳进行区分
    const filename = fileName + time + '.xls'
    const link = document.createElement('a') // 创建a标签
    link.download = filename // 文件名
    link.style.display = 'none'
    link.href = URL.createObjectURL(blob) // 创建URL对象
    document.body.appendChild(link) // 在body中添加a标签
    link.click()
    URL.revokeObjectURL(link.href)  // 主动释放URL对象
    document.body.removeChild(link) // 移除a标签
}