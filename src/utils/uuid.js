/**
 * 全局 创建 UUID
 * @returns 
 */
 export const UUID = () => {
    let __UUID__ = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16)
    })
    return __UUID__.replaceAll('-', '')
}

export default UUID