/**
 * 数组 对象 去重 验证
 * @param {*} query 对象 key值
 * @param {*} key 需要对比的值
 * @param {*} num 当前索引
 * @param {*} list 验证的数组
 * @returns 
 */
const duplicateArray =  (query, key, num, list) => {
    if (!list || list.length == 0) {
        return false
    }
    if (list[num][query] == key) {
        return true
    } else {
        if ((num + 1) == list.length) {
            return false
        }
        return duplicateArray(query, key, num + 1, list)
    }
}

/**
 * 数组去重
 * @param {*} data 
 * @param {*} query 
 * @param {*} key 
 * @param {*} num 
 * @param {*} list 
 * @returns 
 */
const duplicate = (data, query, key, num, list) => {
    if (data && data.length > 0) {
        let newData = list
        data.forEach( (e) => {
            const exist = duplicateArray(query, e[key], num, newData)
            if (!exist) {
                newData.push(e)
            }
        })
        return newData
    }else {
        return new Array
    }
}

export default duplicate