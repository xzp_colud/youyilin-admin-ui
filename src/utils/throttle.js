/**
 * 节流
 */
const throttle = function(func, delay) {
    const _delay = delay || 500
    let timer = null
    let startTime = Date.now()
    return function() {
        // 结束时间
        const curTime = Date.now()
        // 间隔时间 = 延迟的时间 - （结束时间戳 - 开始时间戳）
        const interval = _delay - (curTime - startTime)
        const th = this
        const args = arguments
        clearTimeout(timer)
        if(interval <= 0) {
            // 证明可以触发了
            func.apply(th, args)
            // 重新计算开始时间
            startTime = Date.now()
        } else {
            // 继续等待
            timer = setTimeout(func, interval)
        }
    }
}

export default throttle