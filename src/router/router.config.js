// 基础路由
const constantRouters = [
    //单个路由均为对象类型，path代表的是路径，component代表组件
    {
        path: '/login',
        name: 'LOGIN',
        component: () => import('@/views/Login.vue'),
    },
    {
        path: '/',
        name: 'INDEX',
        redirect: '/index',
        component: () => import('@/views/Index.vue'),
        children: [
            {
                path: '/index',
                name: 'INDEX',
                meta: {
                    pidName: '首页',
                    menuName: '工作台',
                },
                component: () => import('@/views/Dashboard.vue'),
            },
            {
                path: '/user/info',
                name: 'USER_INFO',
                component: () => import('@/views/common/UserInfo.vue'),
            },
            {
                path: '/user/password',
                name: 'USER_PASSWORD',
                component: () => import('@/views/common/Password.vue'),
            },
        ],
    },
    {
        path: '/500',
        name: '500',
        component: () => import('@/views/500.vue'),
    },
    // {
    //     path: '/404',
    //     name: '404',
    //     component: () => import('@/views/404'),
    // },
]

export default constantRouters