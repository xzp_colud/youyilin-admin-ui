import { createRouter, createWebHashHistory } from 'vue-router'

import nprogress from 'nprogress'
import 'nprogress/nprogress.css'

nprogress.configure({ 
    easing: 'ease',  // 动画方式    
    speed: 200,  // 递增进度条的速度    
    showSpinner: false, // 是否显示加载ico    
    trickleSpeed: 1000, // 自动递增间隔    
    minimum: 0.1 // 初始化时的最小百分比
})

import constantRouters from '@/router/router.config'
import { ACCESS_TOKEN } from '@/store/constant'
import useStore from '@/store'

// 404 路由
const router404 = {
    path: '/:pathMatch(.*)',
    name: '404',
    component: () => import('@/views/404'),
}
// 免登陆路由
const allowList = ['LOGIN', '500']
const loginRouterPath = '/login'
// 路由模式
const routerHistory = createWebHashHistory()
 
// 路由
const router = createRouter({
    history: routerHistory,
    routes: constantRouters,
})

// 路由守卫
router.beforeEach((to, from, next) => {
    nprogress.start()
    const pidName = to.meta.pidName ? to.meta.pidName : ''
    const menuName = to.meta.menuName ? to.meta.menuName : ''

    const { commonStore, dictStore, menuStore, themeStore, userStore } = useStore()
    // 应用标题
    const appName = commonStore.app.name
    document.title = commonStore.dynamicTitle === true ? (menuName ? menuName : appName) : appName
    // 设置标题
    commonStore.$patch({ pidName: pidName, menuName: menuName })
    // 设置选中菜单
    menuStore.$patch({ selectedMenu: [to.path] })

    // 应用主题
    if (!themeStore.isLoadingTheme) themeStore.LoadingTheme()

    if (allowList.includes(to.name)) {
        next()
    } else {
        const token = localStorage.getItem(ACCESS_TOKEN)
        if (token) {
            if (userStore.isLoadingUser) {
                next()
            } else {
                // 加载用户
                userStore.GetUserInfo().then(() => {
                    // 加载数据字典
                    if (!dictStore.dictLoading) dictStore.GetDictData()

                    if (menuStore.isAddRouters) {
                        next()
                    } else {
                        // 加载路由菜单
                        menuStore.GenerateRoutes().then(res => {
                            res.forEach(item => {
                                if (item.meta.link) {
                                    router.addRoute(item)
                                } else {
                                    router.addRoute('INDEX', item)
                                }
                            })
                            router.addRoute(router404)
                            next({ ...to, replace: true })
                        })
                    }
                })
                // .catch(() => userStore.Logout().then(() => next({ path: loginRouterPath })))
            }
        } else {
            next({ path: loginRouterPath })
        }
    }
    
    // next()
})

router.afterEach((to, from) => nprogress.done())

export default router