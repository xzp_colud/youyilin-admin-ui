// 图标
import * as Icons from '@/layouts/IconExample'
// JSON 解析
import JsonViewer from 'vue3-json-viewer'
import 'vue3-json-viewer/dist/index.css'
// JSON 解析 自定义弹窗
import JsonView from '@/components/jsonView/JsonView.vue'
// 省市区
import MyAddress from '@/components/address/MyAddress.vue'
// 金额格式化
import MyMoney from '@/components/common/MyMoney.vue'
import MyMoneyDiscount from '@/components/common/MyMoneyDiscount.vue'
// 复制图标
import IconCopy from '@/components/common/IconCopy.vue'
// 打印图标
import IconPrint from '@/components/common/IconPrint.vue'
// 文件大小
import MySizeCount from '@/components/common/MySizeCount.vue'
// 字典翻译
import MyDictTransform from '@/components/common/MyDictTransform.vue'
// 表格
import MyTable from '@/components/table/MyTable.vue'
import AdvanceTable from '@/components/table/AdvanceTable.vue'
// 表格 搜索栏
import MyTableSearch from '@/components/table/MyTableSearch.vue'
import AdvanceTableSearch from '@/components/table/AdvanceTableSearch.vue'
// 上传 图片
import UploadImage from '@/components/upload/UploadImage.vue'
// 上传 文件
import MyUploadFile from '@/components/upload/MyUploadFile.vue'
// 弹窗
import AdvanceModal from '@/components/modal/AdvanceModal.vue'
// 抽屉
import AdvanceDrawer from '@/components/drawer/AdvanceDrawer.vue'
// 弹性布局
import CustomSpace from '@/components/common/CustomSpace.vue'
// 二维码
import MyQr from '@/components/qr/MyQr.vue'
// PDF 浏览
import PdfPreview from '@/components/pdf/PdfPreview.vue'
// 按钮
import MyButton from '@/components/button/MyButton.vue'
import ToolButton from '@/components/button/ToolButton.vue'
// 编辑器
import WangEditor from '@/components/editor/WangEditor.vue'
// 状态及布尔值
import FormItemStatus from '@/components/common/FormItemStatus.vue'
import FormItemDefault from '@/components/common/FormItemDefault.vue'
// 翻译-状态及布尔值
import BadgeStatus from '@/components/common/BadgeStatus.vue'
import TagDefault from '@/components/common/TagDefault.vue'

import ChooseSpaceImage from '@/views/goods/spaceImage/ChooseSpaceImage.vue'

// 全局组件
export default (app) => {
    app.use(JsonViewer)

    // 全局使用图标，遍历引入
    const icons = Icons
    for (const i in icons) {
        if (i.indexOf('Outlined') > 0 || i === 'CopyTwoTone') {
            app.component(i, icons[i])
        }
    }
    app.component('MyAddress', MyAddress)
    app.component('MyMoney', MyMoney)
    app.component('MyMoneyDiscount', MyMoneyDiscount)
    app.component('IconCopy', IconCopy)
    app.component('IconPrint', IconPrint)
    app.component('MySizeCount', MySizeCount)
    app.component('MyDictTransform', MyDictTransform)
    app.component('UploadImage', UploadImage)
    app.component('MyUploadFile', MyUploadFile)
    app.component('MyTable', MyTable)
    app.component('MyTableSearch', MyTableSearch)
    app.component('AdvanceTable', AdvanceTable)
    app.component('AdvanceTableSearch', AdvanceTableSearch)
    app.component('AdvanceModal', AdvanceModal)
    app.component('AdvanceDrawer', AdvanceDrawer)
    app.component('CustomSpace', CustomSpace)
    app.component('MyQr', MyQr)
    app.component('PdfPreview', PdfPreview)
    app.component('MyButton', MyButton)
    app.component('ToolButton', ToolButton)
    app.component('JsonView', JsonView)
    app.component('WangEditor', WangEditor)
    app.component('FormItemStatus', FormItemStatus)
    app.component('FormItemDefault', FormItemDefault)
    app.component('BadgeStatus', BadgeStatus)
    app.component('TagDefault', TagDefault)
    app.component('ChooseSpaceImage', ChooseSpaceImage)
}