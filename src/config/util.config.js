// 时间转换
import time from '@/utils/time'
// 数组过滤
import duplicate from '@/utils/duplicate'
// confirm 提示
import affirm from '@/utils/affirm'
// 页面 uuid
import UUID from '@/utils/uuid'
// 防抖
import debounce from '@/utils/debounce'
// 节流
import throttle from '@/utils/throttle'
// URL 编码
import urlCode from '@/utils/urlCode'

// 全局挂载 Util
export default (app) => {
    app.config.globalProperties.$time = time
    app.config.globalProperties.$duplicate = duplicate
    app.config.globalProperties.$affirm = affirm
    app.config.globalProperties.$uuid = UUID
    app.config.globalProperties.$debounce = debounce
    app.config.globalProperties.$throttle = throttle
    app.config.globalProperties.$urlCode = urlCode
}

import { getCurrentInstance } from 'vue'

export function getCtxProxy () {
    const { proxy } = getCurrentInstance()
    return proxy
}

export function getCustomUtil () {
    const { appContext } = getCurrentInstance()
    return appContext.config.globalProperties
}