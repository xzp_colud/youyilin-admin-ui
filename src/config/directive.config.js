import useStore  from '@/store'

// 按钮权限
const authValidate =  {
    mounted (el, binding) {
        if (!binding.value) return

        const { userStore } = useStore()
        // 已获取的权限
        const auth = userStore.perms
        // 是否存在权限
        const isAuth = Array.isArray(auth) && auth.length > 0
        // 是否验证通过
        if (isAuth && auth.includes(binding.value)) {
            return
        }

        // 删除DOM
        // el.disabled = true
        // el.remove()
    }
}

/**
 * 自定义指令
 */
export default (app) => {
    app.directive('auth', authValidate)

    app.directive('focus', {
        mounted (el) {
            el.focus()
        }
    })
}