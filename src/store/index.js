import useCommonStore from './modules/common'
import useThemeStore from './modules/theme'
import useMenuStore from './modules/menu'
import useUserStore from './modules/user'
import useDictStore from './modules/dict'

export default function useStore() {
  return {
    commonStore: useCommonStore(),
    dictStore: useDictStore(),
    menuStore: useMenuStore(),
    themeStore: useThemeStore(),
    userStore: useUserStore(),
  }
}