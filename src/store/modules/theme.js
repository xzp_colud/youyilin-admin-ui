const constantColor = ['#1890ff', '#f5222d', '#fa541c', '#faad14', '#13c2c2', '#52c41a', '#2f54eb', '#722ed1']
const theme_dark = 'dark'
const theme_light = 'light'

import { defineStore } from 'pinia'
import { theme } from 'ant-design-vue'

const useThemeStore = defineStore('theme', {
    state: () => ({
        isLoadingTheme: false,
        // 全局配置
        theme: {
            token: {
                colorPrimary: constantColor[0],
                borderRadius: 0,
                wireframe: false,
            },
            algorithm: theme.defaultAlgorithm,
        },
        // 本地存储
        local: {
            theme: theme_light,
            primaryColor: constantColor[0],
            borderRadius: 0,
            wireframe: false,
        },
        // 头部
        header: {
            theme: theme_light,
        },
        // 侧边栏
        sider: {
            theme: theme_light,
            subTheme: theme_light,
            width: 135,
            // width: 200,
            collapsedWidth: 50,
            menuInlineWidth: 16,
        },
        // 主题色
        primaryColorArr: constantColor,
    }),
    actions: {
        // 设置风格
        refreshTheme(nVal) {
            this.local.theme = nVal
            this.theme.algorithm = nVal === theme_dark ? theme.darkAlgorithm : theme.defaultAlgorithm
            this.setLocal()
        },
        // 设置主题色
        refreshPrimaryColor(nVal) {
            this.local.primaryColor = nVal
            this.theme.token.colorPrimary = nVal
            this.setLocal()
        },
        // 设置线框风格
        refreshWireframe(nVal) {
            this.local.wireframe = nVal
            this.theme.token.wireframe = nVal
            this.setLocal()
        },
        // 设置圆角大小
        refreshBorderRadio(nVal) {
            this.local.borderRadius = nVal
            this.theme.token.borderRadius = nVal
            this.setLocal()
        },
        // 设置导航栏风格
        refreshHeaderTheme(nVal) {
            this.header.theme = nVal
            this.setLocal()
        },
        // 设置侧边栏
        refreshSiderTheme(nVal) {
            this.sider.theme = nVal
            this.setLocal()
        },
        refreshSiderWidth(nVal) {
            this.sider.width = nVal
            this.setLocal()
        },
        refreshSiderCollpasedWidth(nVal) {
            this.sider.collapsedWidth = nVal
            this.setLocal()
        },
        refreshSiderMenuInlineWidth(nVal) {
            this.sider.menuInlineWidth = nVal
            this.setLocal()
        },
        // 本地存储
        setLocal() {
            const theme = this.local.theme
            const html = document.documentElement
            html.setAttribute('data-doc-theme', theme)
            localStorage.setItem('theme', theme)
        },
        // 加载
        LoadingTheme() {
            try {
                const localTheme = localStorage.getItem('theme')
                if (localTheme) {
                    this.local.theme = localTheme
                    this.header.theme = localTheme
                    this.sider.theme = localTheme
                    this.theme.algorithm = localTheme === theme_dark ? theme.darkAlgorithm : theme.defaultAlgorithm

                    const html = document.documentElement
                    html.setAttribute('data-doc-theme', localTheme)
                }
            } finally {
                this.isLoadingTheme = true
            }
        },
        // 重置
        resetTheme() {
            this.$reset()
        },
    }
})

export default useThemeStore