import { listDictDataByDictTypeList } from '@/service/system/dictData'

const dictTypeList = [
    'common_status_enum',
    'common_boolean',
    'common_redisKeys_enum',
    'system_menuType_enum',
    'system_noticeReadStatus_enum',

    // 客户相关
    'customer_charge_amount',
    'customer_charge_pay_type',
    'customer_charge_remark',
    // 物流公司
    'logistics_company',
    // 订单相关
    'order_type',
    // 产品相关
    'product_other_card_category_ids',
]

import { defineStore } from 'pinia'

const useDictStore = defineStore('dict', {
    state: () => ({
        // 是否已经加载
        dictLoading: false,
        // 全局基础状态
        commonStatus: [],
        // 全局布尔值
        commonBoolean: [],
        // Redis 所有key值
        commonRedisKeys: [],
        // 菜单类型
        systemMenuType: [],
        // 通知状态
        systemRoticeReadStatus: [],

        // 客户相关
        customerChargeAmount: [],
        customerChargePayType: [],
        customerChargeRemark: [],
        // 物流公司
        logisticsCompany: [],
        // 订单相关
        orderType: [],
        // 产品相关
        productOtherCardCategoryIds: [],
    }),
    actions: {
        GetDictData() {
            this.dictLoading = true
            listDictDataByDictTypeList(dictTypeList).then(res => {
                const data = res.data

                // 全局布尔值
                let commonBoolean = data.common_boolean
                if (Array.isArray(commonBoolean) && commonBoolean.length >= 2) {
                    commonBoolean.forEach(item => {
                        item.label = item.dictLabel
                        item.value = item.dictValue
                    })
                } else {
                    commonBoolean = [
                        { label: '是', value: '1', dictLabel: '是', dictValue: '1' },
                        { label: '否', value: '0', dictLabel: '否', dictValue: '0' },
                    ]
                }

                // 全局基础状态
                let commonStatus = data.common_status_enum
                if (commonStatus) {
                    commonStatus.forEach(item => {
                        item.label = item.dictLabel
                        item.value = item.dictValue
                    })
                } else {
                    commonStatus = [
                        { label: '正常', value: '1', dictLabel: '正常', dictValue: '1' },
                        { label: '禁用', value: '0', dictLabel: '禁用', dictValue: '0' },
                    ]
                }
                this.commonBoolean = commonBoolean
                this.commonStatus = commonStatus
                this.commonRedisKeys = Array.isArray(data.common_redisKeys_enum) ? data.common_redisKeys_enum : []
                this.systemMenuType = Array.isArray(data.system_menuType_enum) ? data.system_menuType_enum : []
                this.systemRoticeReadStatus = Array.isArray(data.system_noticeReadStatus_enum) ? data.system_noticeReadStatus_enum : []

                this.logisticsCompany = Array.isArray(data.logistics_company) ? data.logistics_company : []
                this.Customer(data)
                this.Order(data)
                this.Product(data)
            })
        },
        // 客户相关
        Customer(data) {
            this.customerChargeAmount = Array.isArray(data.customer_charge_amount) ? data.customer_charge_amount : []
            this.customerChargePayType = Array.isArray(data.customer_charge_pay_type) ? data.customer_charge_pay_type : []
            this.customerChargeRemark = Array.isArray(data.customer_charge_remark) ? data.customer_charge_remark : []
        },
        // 订单相关
        Order(data) {
            this.orderType = Array.isArray(data.order_type) ? data.order_type : []
        },
        // 产品相关
        Product(data) {
            this.productOtherCardCategoryIds = Array.isArray(data.product_other_card_category_ids) ? data.product_other_card_category_ids : []
        }
    }
})

export default useDictStore