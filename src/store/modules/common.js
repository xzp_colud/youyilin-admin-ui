import { defineStore } from 'pinia'
import logo from '@/assets/logo.jpg'

const useCommonStore = defineStore('common', {
    state: () => ({
        app: {
            // 应用名称
            name: 'YOUYILIN',
            // 应用LOGO
            logo: logo,
            // 应用头像
            avatar: '',
            // 备案号
            recordNumber: '浙ICP备2023012613号-1',
            // 版权信息
            copyright: '',
        },
        // 动态标题
        dynamicTitle: true,
        
        // 全局遮罩 Loading
        loading: false,
        // 全局Table loading
        loadingTable: false,
        // 路由根目录
        pidName: '',
        // 路由标题
        menuName: '',
    }),
})

export default useCommonStore