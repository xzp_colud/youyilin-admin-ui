import constantRouters from '@/router/router.config'
import { groupBy } from '@/utils/util'
import { getRouters } from '@/service/system/menu'
import { defineStore } from 'pinia'

const moduleR = import.meta.glob(`@/views/**/*.vue`)

const useMenuStore = defineStore('menu', {
  state: () => ({
    // 基础的路由
    constantRouters: constantRouters,
    // 需要动态添加的路由
    addRouters: [],
    // 是否已经添加过路由
    isAddRouters: false,
    // 所有的菜单
    menus: [],
    // 选中的一级菜单
    selectedMenu: null,
    // 二级菜单
    subMenus: [],
    // 选中的二级菜单
    selectedSubMenu: null,
  }),
  actions: {
    // 构建动态路由
    GenerateRoutes() {
      return new Promise(resolve => {
        getRouters().then(res => {
          const routersData = res.data.filter(item => item.path).map(item => {
            return {
              path: item.path,
              name: item.name,
              // component: (() => import(`@/views${item.component}`)),
              component: moduleR[`/src/views${item.component}.vue`],
              meta: {
                requireAuth: true,
                pidName: item.pidName,
                menuName: item.menuName,
                link: item.link === 1,
              }
            }
          })
          
          // 过滤不显示的菜单
          const filterVisibleMenu = res.data.filter(item => item.visible === 1)
          // 按父级分组
          const groupMenu = groupBy(filterVisibleMenu, 'pidId')
          let userMenu = []
          if (typeof groupMenu === 'object') {
            // 构建菜单树
            const parentMenu = groupMenu['0']
            if (parentMenu && parentMenu.length > 0) {
              asyncGenerateMenu(parentMenu, groupMenu)
              userMenu = userMenu.concat(parentMenu)
            } else {
              userMenu = []
            }
          }
          
          this.routers = routersData
          this.menus = userMenu
          resolve(routersData)
        })
      })
    },
    // 加载搜索菜单
    // LoadingSearchMenu() {
    //   return new Promise((resove, reject) => {
    //     getSearch().then(res => {
    //       const data = res.data
    //       if (Array.isArray(data)) resove(data)
    //       else reject(res)
    //     }).catch(res => reject(res))
    //   })
    // }
  }
})

// 构建子菜单
function asyncGenerateMenu(parentMenu, groupMenu) {
  parentMenu.forEach(item => {
    const childrenMenu = groupMenu[item.id]
    item.children = childrenMenu
    if (Array.isArray(childrenMenu) && childrenMenu.length > 0) {
      asyncGenerateMenu(childrenMenu, groupMenu)
    }
  })
}

export default useMenuStore