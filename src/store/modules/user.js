import { doLogin, doPhoneLogin, getLoginUser, doLogout, isLogin } from '@/service/login'
import { ACCESS_TOKEN } from '@/store/constant'

import { defineStore } from 'pinia'

const useUserStore = defineStore('user', {
    state: () => ({
        // 是否已经加载用户信息
        isLoadingUser: false,
        // TOKEN 凭证
        token: '',
        // 用户
        userName: '',
        // 头像
        avatarUrl: '',
        // 真实姓名
        realName: '',
        // 工号
        jobNumber: '',
        // 登录时间
        loginDate: '',
        // 登录IP
        loginIp: '',
        // 登录地点
        loginLocation: '',
        // 按钮权限
        perms: [],
    }),
    actions: {
        // 账户密码登录
        Login(loginForm) {
            return new Promise((resolve, reject) => {
                doLogin(loginForm).then(res => {
                    this.token = res.data.token
                    resolve(res)
                }).catch(error => reject(error))
            })
        },
        // 账户密码登录
        LoginPhone(loginForm) {
            return new Promise((resolve, reject) => {
                doPhoneLogin(loginForm).then(res => {
                    this.token = res.data.token
                    resolve(res)
                }).catch(error => reject(error))
            })
        },
        // 用户信息
        GetUserInfo() {
            return new Promise((resolve, reject) => {
                getLoginUser().then(res => {
                    if (res.data) {
                        this.isLoadingUser = true
                        this.userName = res.data.userName
                        this.avatarUrl = res.data.avatarUrl
                        this.realName = res.data.realName
                        this.jobNumber = res.data.jobNumber
                        this.loginDate = res.data.loginDate
                        this.loginIp = res.data.loginIp
                        this.perms = res.data.perms
                        resolve(res)
                    } else {
                        reject(res)
                    }
                }).catch(error => reject(error))
            })
        },
        // 退出
        Logout() {
            return new Promise(resolve => {
                doLogout().finally(() => {
                    resolve()
                    this.isLoadingUser = false
                    localStorage.removeItem(ACCESS_TOKEN)
                })
            })
        },
        // 是否已经登录
        isLogin() {
            return new Promise((resolve, reject) => {
                const token = localStorage.getItem(ACCESS_TOKEN)
                if (token) {
                    isLogin().then(res => resolve(res)).catch(() => reject())
                } else {
                    reject()
                }
            })
        }
    }
})

export default useUserStore