export const configType = [
    { id: 1, text: '单行文本' },
    { id: 2, text: '多行文本' },
    { id: 3, text: '密码' },
    { id: 4, text: '数字' },
    { id: 5, text: '多选框', isOptions: true },
    { id: 6, text: '单选框', isOptions: true },
    { id: 7, text: '日期' },
    { id: 8, text: '图片' },
    { id: 9, text: '下拉框', isOptions: true },
]