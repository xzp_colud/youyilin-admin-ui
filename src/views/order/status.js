// 采购单 订单状态
export const purchase_order_status = [
     { dictLabel: '已取消', dictValue: '0', tableClass: 'default' }
    ,{ dictLabel: '待确认', dictValue: '1', tableClass: 'processing' }
    ,{ dictLabel: '入库中', dictValue: '30', tableClass: 'processing' }
    ,{ dictLabel: '完成', dictValue: '50', tableClass: 'success' }
]

// 采购单 子集状态
export const purchase_order_item_status = [
    { dictLabel: '已取消', dictValue: '0', tableClass: 'default' }
   ,{ dictLabel: '待确认', dictValue: '1', tableClass: 'processing' }
   ,{ dictLabel: '待收货', dictValue: '10', tableClass: 'warning' }
   ,{ dictLabel: '检验中', dictValue: '20', tableClass: 'warning' }
   ,{ dictLabel: '入库中', dictValue: '30', tableClass: 'processing' }
   ,{ dictLabel: '已入库', dictValue: '40', tableClass: 'success' }
]

// 出库单 订单状态
export const outbound_order_status = [
     { dictLabel: '已取消', dictValue: '0', tableClass: 'warning' }
    ,{ dictLabel: '待确认', dictValue: '1', tableClass: 'default' }
    ,{ dictLabel: '出库中', dictValue: '2', tableClass: 'processing' }
    ,{ dictLabel: '已出库', dictValue: '40', tableClass: 'success' }
]

// 出库单 子集状态
export const outbound_order_item_status = [
     { dictLabel: '已取消', dictValue: '0', tableClass: 'warning' }
    ,{ dictLabel: '待确认', dictValue: '1', tableClass: 'default' }
    ,{ dictLabel: '出库中', dictValue: '20', tableClass: 'processing' }
    ,{ dictLabel: '已出库', dictValue: '30', tableClass: 'success' }
]

// 销售单 状态
export const sale_order_status = [
    { dictLabel: '已退款', dictValue: '-1', tableClass: 'error' }
   ,{ dictLabel: '已取消', dictValue: '0', tableClass: 'default' }
   ,{ dictLabel: '未提交', dictValue: '1', tableClass: 'warning' }
   ,{ dictLabel: '待付款', dictValue: '10', tableClass: 'processing' }
   ,{ dictLabel: '待出库', dictValue: '20', tableClass: 'processing' }
   ,{ dictLabel: '出库中', dictValue: '21', tableClass: 'processing' }
   ,{ dictLabel: '待发货', dictValue: '30', tableClass: 'processing' }
   ,{ dictLabel: '发货中', dictValue: '31', tableClass: 'processing' }
   ,{ dictLabel: '已发货', dictValue: '50', tableClass: 'success' }
   ,{ dictLabel: '已完成', dictValue: '60', tableClass: 'success' }
   ,{ dictLabel: '待备货', dictValue: '15', tableClass: 'processing' }
   ,{ dictLabel: '备货中', dictValue: '16', tableClass: 'processing' }
   ,{ dictLabel: '已备货', dictValue: '61', tableClass: 'success' }
]

// 销售单 子集状态
export const sale_order_item_status = [
    { dictLabel: '已退款', dictValue: '-1', tableClass: 'error' }
   ,{ dictLabel: '已取消', dictValue: '0', tableClass: 'default' }
   ,{ dictLabel: '未提交', dictValue: '1', tableClass: 'warning' }
   ,{ dictLabel: '待付款', dictValue: '10', tableClass: 'processing' }
   ,{ dictLabel: '未出库', dictValue: '20', tableClass: 'processing' }
   ,{ dictLabel: '出库中', dictValue: '21', tableClass: 'processing' }
   ,{ dictLabel: '已出库,待发货', dictValue: '30', tableClass: 'processing' }
   ,{ dictLabel: '发货中', dictValue: '31', tableClass: 'processing' }
   ,{ dictLabel: '已发货', dictValue: '50', tableClass: 'success' }
   ,{ dictLabel: '待备货', dictValue: '15', tableClass: 'processing' }
   ,{ dictLabel: '备货中', dictValue: '16', tableClass: 'processing' }
   ,{ dictLabel: '已备货', dictValue: '61', tableClass: 'success' }
]

// 销售单 付款记录 状态
export const sale_order_collect_status = [
     { dictLabel: '退款', dictValue: '-1', tableClass: 'error' }
    ,{ dictLabel: '未核验', dictValue: '1', tableClass: 'processing' }
    ,{ dictLabel: '付款成功', dictValue: '10', tableClass: 'success' }
    ,{ dictLabel: '付款失败', dictValue: '20', tableClass: 'error' }
]

// 销售单 退款记录 状态
export const sale_order_refund_status = [
    { dictLabel: '未退款', dictValue: '1', tableClass: 'default' }
   ,{ dictLabel: '退款中', dictValue: '10', tableClass: 'processing' }
   ,{ dictLabel: '已取消', dictValue: '20', tableClass: 'error' }
   ,{ dictLabel: '已退款', dictValue: '30', tableClass: 'success' }
]

// 结款单 状态 状态
export const settled_order_status = [
     { dictLabel: '未确认', dictValue: '1', tableClass: 'default' }
    ,{ dictLabel: '已取消', dictValue: '2', tableClass: 'warning' }
    ,{ dictLabel: '收款中', dictValue: '10', tableClass: 'processing' }
    ,{ dictLabel: '已收款', dictValue: '20', tableClass: 'success' }
]

// 退款单 状态
export const refund_order_status = [
    { dictLabel: '待处理', dictValue: 'WAITING', tableClass: 'processing' }
    ,{ dictLabel: '成功', dictValue: 'SUCCESS', tableClass: 'success' }
    ,{ dictLabel: '失败', dictValue: 'FAIL', tableClass: 'error' }
]