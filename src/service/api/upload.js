import request from "@/utils/request"

// 上传文件
export const uploadFile = (file, fileGroup) => request.post('/upload/file', { 
    file: file,
    fileGroup: fileGroup 
}, { 
    loading: false, 
    headers: { 
        'Content-Type': 'multipart/form-data' 
    }
})