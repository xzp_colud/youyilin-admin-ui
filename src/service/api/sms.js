import request from "@/utils/request"

// 短信验证码
export const sendSmsCode = data => request.post('/api/smsCode', data, { loading: false, form: true })