import request from "@/utils/request"

const baseModule = '/address/'

// 列表
export const getPageList = params => request.get(baseModule + 'getPageList', params)

// 获取省份
export const provinceName = params => request.get(baseModule + 'province/' + params)

// 获取下级地区
export const getNext = params => request.get(baseModule + 'next/' + params)

// 树型
export const findTree = () => request.get(baseModule + 'listTree')
