import request from "@/utils/request"

const baseModule = '/api/qrcode'

// 创建
export const createQr = data => request.post(baseModule + '/created', data)

// 查询
export const findQr = data => request.post(baseModule + '/getQrCode/' + data)
