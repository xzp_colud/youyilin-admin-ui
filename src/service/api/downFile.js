import { message } from 'ant-design-vue'
import axios from 'axios'

const baseUrl = import.meta.env.VITE_APP_BASE_URL

// 下载ZIP文件
export const downZipFile = (url, data) => {
    axios({
        method: 'POST',
        url: baseUrl + url,
        data: data,
        headers: {
            Authorization: localStorage.getItem('token'),
            timestamp: new Date().getTime(),
        },
        responseType: 'blob',
    }).then(response => {
        if (response.data) {
            download(response)
        }
    })
}


// 下载EXCEL文件
const excelType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
export const downExcelFile = (url, data) => {
    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: baseUrl + url,
            data: data,
            headers: {
                Authorization: localStorage.getItem('token'),
                timestamp: new Date().getTime(),
            },
            responseType: 'blob',
        }).then(response => {
            const _data = response.data
            if (_data && _data.type === excelType) {
                download(response, excelType)
                resolve()
            } else if (_data.size > 0) {
                message.success('操作成功')
                resolve()
            } else {
                message.error('操作异常')
                reject()
            }
        }).catch(() => {
            message.error('导出异常')
            reject()
        })
    })
}

const download = (res, blobType) => {
    const data = res.data
    const url = window.URL.createObjectURL(new Blob([data], { type: blobType }))
    const filename = window.decodeURI(res.headers['content-disposition'].split('=')[1])
    const link = document.createElement('a')
    link.style.display = 'none'
    link.href = url
    link.setAttribute('download', filename.replace('utf-8\'\'', ''))
     
    document.body.appendChild(link)
    link.click()
}