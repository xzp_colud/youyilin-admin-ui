import request from "@/utils/request"

// 图片验证码
export const captchaImage = () => request.post('/api/captchaImage', {}, { loading: false })
