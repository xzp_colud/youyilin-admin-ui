import request from "@/utils/request"

const baseModule = '/customer.address'

// 列表
export const customerAddressPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getCustomerAddress = params => request.get(baseModule + '/queryEdit/' + params)

// 客户地址
export const listCustomerAddress = params => request.get(baseModule + '/listByCustomerId/' + params)

// 新增
export const customerAddressAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const customerAddressEdit = data => request.post(baseModule + '/edit', data)

// 默认
export const customerAddressDefaults = data => request.post(baseModule + '/defaults', data)

// 删除
export const customerAddressDel = data => request.post(baseModule + '/del', data)