import request from "@/utils/request"

const baseModule = '/customer.account.log'

// 列表
export const customerAccountLogPage = params => request.get(baseModule + '/getPageList', { params: params })