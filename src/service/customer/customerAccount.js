import request from "@/utils/request"

const baseModule = '/customer.account'

// 列表
export const customerAccountPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const listCustomerAccount = params => request.get(baseModule + '/listByCustomerId/' + params)

// 新增
export const customerAccountAdd = data => request.post(baseModule + '/add', data)

// 充值
export const customerAccountCharge = data => request.post(baseModule + '/charge', data)