import request from "@/utils/request"

const baseModule = '/customer'

// 列表
export const customerPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getCustomerDetail = params => request.get(baseModule + '/queryDetail/' + params)

// 查询
export const getCustomerEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const customerAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const customerEdit = data => request.post(baseModule + '/edit', data)