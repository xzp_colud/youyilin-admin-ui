import request from "@/utils/request"

const baseModule = '/customer.mini'

// 列表
export const customerMiniPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getCustomerMini = params => request.get(baseModule + '/getMini/' + params)