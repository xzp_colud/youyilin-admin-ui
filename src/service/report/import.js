import request from "@/utils/request"

// 导入菜单
export const menuImport = (file) => {
    const formData = new FormData()
    formData.append('file', file)

    return request({
        url: '/import.system/menu',
        method: 'POST',
        data: formData,
        loading: true,
        form: true,
    })
}