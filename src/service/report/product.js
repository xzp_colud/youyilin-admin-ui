import { downExcelFile } from '@/service/api/downFile'

// 导入产品
export const importProduct = (file) => {
    const formData = new FormData()
    formData.append('file', file)

    return downExcelFile('/import.product/insert', formData, { loading: true })
}

// 导入产品(更新)
export const importProductUpdate = (file) => {
    const formData = new FormData()
    formData.append('file', file)

    return downExcelFile('/import.product/update', formData, { loading: true })
}

// 导入产品属性
export const importProductParasms = (file) => {
    const formData = new FormData()
    formData.append('file', file)

    return downExcelFile('/import.product/params', formData, { loading: true })
}

// 导入产品状态
export const importProductStatus = (file) => {
    const formData = new FormData()
    formData.append('file', file)

    return downExcelFile('/import.product/status', formData, { loading: true })
}

// 导出产品
export const exportProduct = (data) => downExcelFile('/export.product/excel', data)

// 导出产品属性
export const exportProductParams = (data) => downExcelFile('/export.product/params', data)

// 导出产品SKU
export const exportProductSKU = (data) => downExcelFile('/export.product/sku', data)