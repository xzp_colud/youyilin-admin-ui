import { downExcelFile } from '@/service/api/downFile'

// 导出菜单
export const menuExport = data => downExcelFile('/export.system/menu', data)