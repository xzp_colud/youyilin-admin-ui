import { downExcelFile } from '@/service/api/downFile'

// 库存信息导出
export const exportInventoryExcel = data => downExcelFile('/export.inventory/excel', data)

// 盘点商品导出
export const exportInventoryCheck = () => downExcelFile('/export.inventory/check')

// 盘点商品导入
export const importInventoryCheck = _file => {
    const formData = new FormData()
    formData.append('file', _file)

    return downExcelFile('/import.inventory/check', formData, { loading: true })
}