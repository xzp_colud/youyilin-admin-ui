import { downExcelFile } from '@/service/api/downFile'

// 采购单导出
export const exportPurchaseExcel = data => downExcelFile('/export.purchase/excel', data)