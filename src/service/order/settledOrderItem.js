import request from "@/utils/request"

const baseModule = '/settled.order.item'

// 根据结款单查询
export const listSettledItemBySettledId = params => request.get(baseModule + '/listBySettledId/' + params)