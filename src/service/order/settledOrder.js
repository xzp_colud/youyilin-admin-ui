import request from "@/utils/request"

const baseModule = '/settled.order'

// 列表
export const pageSettledOrder = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getSettledOrderLastBalance = params => request.get(baseModule + '/getLastBalance/' + params)

// 查询
export const getSettledOrderDetail = params => request.get(baseModule + '/queryDetail/' + params)

// 新增
export const addSettledOrder = data => request.post(baseModule + '/save', data)

// 确认
export const confirmSettledOrder = data => request.post(baseModule + '/confirm', data)

// 取消
export const cancelSettledOrder = data => request.post(baseModule + '/cancel', data)

// 完成
export const finishSettledOrder = data => request.post(baseModule + '/finish', data)