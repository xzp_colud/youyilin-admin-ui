import request from "@/utils/request"

const baseModule = '/order.other.amount'

// 其他费用列表
export const listOtherAmountByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)