import request from "@/utils/request"

const baseModule = '/order.refund'

// 列表
export const orderRefundPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getOrderRefundCheck = params => request.get(baseModule + '/queryCheck/' + params)

// 查询
export const getOrderRefundDetail = params => request.get(baseModule + '/queryDetail/' + params)

// 新增
export const addOrderRefund = data => request.post(baseModule + '/add', data)

// 审核
export const checkOrderRefund = data => request.post(baseModule + '/check', data)