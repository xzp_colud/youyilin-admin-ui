import request from "@/utils/request"

const baseModule = '/order.log'

// 列表
export const orderLogPage = params => request.get(baseModule + '/getPageList', { params: params })