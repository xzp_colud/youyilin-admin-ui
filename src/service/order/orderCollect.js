import request from "@/utils/request"

const baseModule = '/order.collect'

// 列表
export const orderCollectPage = params => request.get(baseModule + '/getPageList', { params: params })