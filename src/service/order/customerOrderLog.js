import request from "@/utils/request"

const baseModule = '/customer.order.log'

// 按订单查询
export const listOrderLogByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)