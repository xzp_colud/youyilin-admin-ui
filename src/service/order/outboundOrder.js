import request from "@/utils/request"

const baseModule = '/outbound.order'

// 列表
export const outboundOrderPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getOutboundOrder = params => request.get(baseModule + '/queryDetail/' + params)

// 查询
export const getOutboundOrderEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const outboundOrderAdd = data => request.post(baseModule + '/add', data)

// 确认
export const outboundOrderConfirm = data => request.post(baseModule + '/confirm', data)

// 出库
export const outboundOrderInventoryOut = data => request.post(baseModule + '/inventoryOut', data)

// 取消
export const outboundOrderCancel = data => request.post(baseModule + '/cancel', data)