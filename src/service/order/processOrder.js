import request from "@/utils/request"

const baseModule = '/process.order'

// 列表
export const processOrderPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getProcessOrder = params => request.get(baseModule + '/search/' + params)

// 查询
export const getProcessOrderEdit = params => request.get(baseModule + '/searchEdit/' + params)

// 新增
export const processOrderAdd = data => request.post(baseModule + '/add', data)

// 确认
export const processOrderConfirm = data => request.post(baseModule + '/confirm', data)

// 出库
export const processOrderInventoryOut = data => request.post(baseModule + '/inventoryOut', data)

// 入库
export const processOrderInventoryIn = data => request.post(baseModule + '/inventoryIn', data)