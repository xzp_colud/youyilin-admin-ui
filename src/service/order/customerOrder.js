import request from "@/utils/request"

const baseModule = '/customer.order'

// 列表
export const customerOrderPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询状态
export const listOrderStatus = params => request.get(baseModule + '/listStatus', { params: params })

// 查询状态
export const listOutboundStatus = params => request.get(baseModule + '/listOutboundStatus', { params: params })

// 查询
export const getCustomerOrder = params => request.get(baseModule + '/getOrder/' + params)

// 申请出库
export const updateCustomerOrderOutbound = data => request.post(baseModule + '/outbound', data)

// 申请发货
export const updateCustomerOrderSend = data => request.post(baseModule + '/send', data)

// 修改地址
export const updateCustomerOrderAddress = data => request.post(baseModule + '/updateAddress', data)