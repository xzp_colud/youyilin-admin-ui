import request from "@/utils/request"

const baseModule = '/order'

// 总列表
export const orderPage = params => request.get(baseModule + '/getPageList', { params: params })

// 待发货列表
export const orderSendPage = params => request.get(baseModule + '/getSendPageList', { params: params })

// 查询
export const getOrderAddress = params => request.get(baseModule + '/queryAddress/' + params)

// 查询
export const getOrder = params => request.get(baseModule + '/queryDetail/' + params)

// 查询
export const getOrderEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 日常订单
export const orderAddDaily = data => request.post(baseModule + '/addDaily', data)

// 新增日常销售单
export const orderAdd = data => request.post(baseModule + '/add', data)

// 新增批发销售单
export const orderAddWholesale = data => request.post(baseModule + '/addWholesale', data)

// 订单提交
export const updateOrderSubmit = data => request.post(baseModule + '/submit', data)

// 订单取消
export const updateOrderCancel = data => request.post(baseModule + '/cancel', data, { form: true })

// 订单付款
export const updateOrderPay = data => request.post(baseModule + '/pay', data)

// 订单商品出库
export const updateOrderInventoryOut = data => request.post(baseModule + '/inventoryOut', data)

// 订单发货
export const updateOrderSend = data => request.post(baseModule + '/send', data)

// 订单超时取消
export const updateOrderTimeOut = data => request.post(baseModule + '/timeOut', data, { form: true })

// 订单收货地址
export const updateOrderAddress = data => request.post(baseModule + '/address', data)

// 获取结款列表
export const listSettledOrderPage = () => request.get(baseModule + '/listSettledPage')

// 转原料采购
export const doOrderToPurchase = data => request.post(baseModule + '/purchase', data)

// 原料库存信息查询
export const getOrderToPurchaseInfo = params => request.get(baseModule + '/getPurchaseInfo', {params: params})

// 转备货采购
export const doOrderToReadyPurchase = data => request.post(baseModule + '/readyPurchase', data)

// 备货库存信息查询
export const getOrderToReadyPurchaseInfo = params => request.get(baseModule + '/getReadyPurchaseInfo', {params: params})