import request from "@/utils/request"

const baseModule = '/customer.order.item'

// 按订单查询
export const listOrderItemByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)