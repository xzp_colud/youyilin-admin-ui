import request from "@/utils/request"

const baseModule = '/order.discount'

// 优惠明细列表
export const listOrderDiscountByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)