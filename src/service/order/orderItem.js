import request from "@/utils/request"

const baseModule = '/order.item'

// 列表
export const listOrderItemByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)