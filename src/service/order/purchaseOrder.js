import request from "@/utils/request"

const baseModule = '/purchase.order'

// 列表
export const purchaseOrderPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getPurchaseOrder = params => request.get(baseModule + '/queryDetail/' + params)

// 查询
export const getPurchaseOrderEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const purchaseOrderAdd = data => request.post(baseModule + '/add', data)

// 确认
export const purchaseOrderConfirm = data => request.post(baseModule + '/confirm', data)

// 检验
export const purchaseOrderCheck = data => request.post(baseModule + '/check', data)

// 批量入库
export const purchaseOrderInventoryInBatch = data => request.post(baseModule + '/inventoryInBatch', data)

// 入库
export const purchaseOrderInventoryIn = data => request.post(baseModule + '/inventoryIn', data)

// 取消
export const purchaseOrderCancel = data => request.post(baseModule + '/cancel', data)