import request from "@/utils/request"

const baseModule = '/customer.order.finance'

// 按订单查询
export const listOrderFinanceByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)