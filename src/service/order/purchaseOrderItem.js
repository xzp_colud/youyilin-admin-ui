import request from "@/utils/request"

const baseModule = '/purchase.order.item'

// 某采购订单商品
export const listPurchaseOrderItemByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)

// 修正实际检验数量
export const updatePurchaseItemReviseNum = data => request.post(baseModule + '/revise', data)