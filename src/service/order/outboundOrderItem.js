import request from "@/utils/request"

const baseModule = '/outbound.order.item'

// 某采购订单商品
export const listOutboundOrderItemByOrderId = params => request.get(baseModule + '/listByOrderId/' + params)