import request from "@/utils/request"

const baseModule = '/waybill.sender'

// 列表
export const waybillSenderPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const listWaybillSender = params => request.get(baseModule + '/list')

// 查询
export const getWaybillSender = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const waybillSenderAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const waybillSenderEdit = data => request.post(baseModule + '/edit', data)