import request from "@/utils/request"

const baseModule = '/waybill'

// 列表
export const waybillPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getWaybill = params => request.get(baseModule + '/query/' + params)

// 查询
export const getWaybillDetail = params => request.get(baseModule + '/queryDetail/' + params)

// 确认
export const sureWaybill = data => request.post(baseModule + '/sure', data)

// 提交信息
export const submitWaybill = data => request.post(baseModule + '/submit', data)