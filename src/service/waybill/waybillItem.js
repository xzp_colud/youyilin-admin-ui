import request from "@/utils/request"

const baseModule = '/waybill.item'

// 查询
export const listWaybillItemByWaybillId = params => request.get(baseModule + '/listByWaybillId/' + params)