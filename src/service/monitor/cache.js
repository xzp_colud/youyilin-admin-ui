import request from "@/utils/request"

const baseModule = '/monitor.cache'

// 列表
export const listKeysByPrefix = params => request.get(baseModule + '/listKeysByPrefix/' + params)

// 查询缓存内容
export const getObjectByKey = data => request.post(baseModule + '/getObjectByKey', data, { loading: false, form: true })

// 删除缓存Key
export const delObjectByKey = data => request.post(baseModule + '/delObjectByKey', data, { form: true })