import request from "@/utils/request"

const baseModule = '/monitor.online'

// 列表
export const onlineList = () => request.get(baseModule + '/list')

// 强退
export const onlineLogout = data => request.post(baseModule + '/logout/' + data)