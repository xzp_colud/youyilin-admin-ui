import request from "@/utils/request"

const baseModule = '/quartz.job'

// 列表
export const jobPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getJob = params => request.get(baseModule + '/' + params)

// 新增
export const jobAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const jobEdit = data => request.post(baseModule + '/edit', data)

// 删除
export const jobDel = data => request.post(baseModule + '/del', data)

// 立即执行
export const jobRun = data => request.post(baseModule + '/run', data)