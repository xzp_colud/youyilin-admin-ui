import request from "@/utils/request"

const baseModule = '/quartz.job.log'

// 列表
export const jobLogPage = params => request.get(baseModule + '/getPageList', { params: params })

// 清空
export const jobLogClean = data => request.post(baseModule + '/clean/' + data)