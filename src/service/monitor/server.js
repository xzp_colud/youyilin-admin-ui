import request from "@/utils/request"

const baseModule = '/monitor.server'

// 列表
export const serverList = () => request.get(baseModule + '/list')