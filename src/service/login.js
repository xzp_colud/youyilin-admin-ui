import request from "@/utils/request"

// 密码登录
export const doLogin = data => request.post('/login', data, { loading: false, form: true })

// 手机短信登录
export const doPhoneLogin = data => request.post('/login/phone', data, { loading: false, form: true })

// 微信登录
// export const doWxLogin = (openid) => {
//     let data = new FormData()
//     data.append('openid', openid)

//     return request({
//         url: '/wxLogin',
//         method: 'POST',
//         data: data,
//         loading: false,
//         form: true,
//     })
// }
export const doWxLogin = (openid) => request.post('/wxLogin', { openid: openid }, { loading: false, form: true })

// 是否已经登录
export const isLogin = () => request.get('/api/isLogin')

// 用户登录信息
export const getLoginUser = () => request.get('/getLoginUser')

// 用户登录信息
export const getUserInfo = () => request.get('/getUserInfo')

// 更新用户信息
export const updateUser = data => request.post('/updateUser', data, { loading: false })

// 修改密码
export const changePassword = data => request.post('/changePassword', data, { loading: false })

// 退出
export const doLogout = () => request.post('/logout')