import request from "@/utils/request"

const baseModule = '/website.image'

// 列表
export const websiteImagePage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getWebsiteImage = params => request.get(baseModule + '/query/' + params)

// 新增
export const websiteImageAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const websiteImageEdit = data => request.post(baseModule + '/edit', data)

// 编辑
export const websiteImageDel = data => request.post(baseModule + '/del', data)