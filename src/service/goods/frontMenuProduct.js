import request from "@/utils/request"

const baseModule = '/front.menu.product'

// 列表
export const frontMenuProductPage = params => request.get(baseModule + '/listPageByFrontMenuId/' + params)

// 保存
export const frontMenuProductSave = data => request.post(baseModule + '/save', data)