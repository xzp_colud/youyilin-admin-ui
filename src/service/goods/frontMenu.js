import request from "@/utils/request"

const baseModule = '/front.menu'

// 列表
export const frontMenuPage = params => request.get(baseModule + '/listPage', { params: params })

// 查询
export const getFrontMenu = params => request.get(baseModule + '/' + params)

// 新增
export const frontMenuAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const frontMenuEdit = data => request.post(baseModule + '/edit', data)

// 修改状态
export const updateFrontMenuStatus = data => request.post(baseModule + '/updateStatus', data)