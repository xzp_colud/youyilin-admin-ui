import request from "@/utils/request"

const baseModule = '/goods.space.image'

// 列表
export const spaceImagePage = params => request.get(baseModule + '/getPageList', { params: params })

// 新增
export const spaceImageAdd = data => request.post(baseModule + '/add', data)

// 删除
export const spaceImageDel = data => request.post(baseModule + '/del', data)