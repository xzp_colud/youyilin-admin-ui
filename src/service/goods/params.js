import request from "@/utils/request"

const baseModule = '/goods.params'

// 列表
export const paramsPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getParams = params => request.get(baseModule + '/query/' + params)

// 所有
export const listAllParams = params => request.get(baseModule + '/listAll')

// 新增
export const paramsAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const paramsEdit = data => request.post(baseModule + '/edit', data)

// 按商品分类查询
export const listParamsByCategoryId = params => request.get(baseModule + '/listByCategoryId/' + params)