import request from "@/utils/request"

const baseModule = '/goods.category'

// 列表
export const categoryPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getCategory = params => request.get(baseModule + '/query/' + params)

// 查询
export const getCategoryEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 查询
export const getCategoryDetail = params => request.get(baseModule + '/queryDetail/' + params)

// 新增
export const categoryAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const categoryEdit = data => request.post(baseModule + '/edit', data)

// 查询
export const categoryAll = params => request.get(baseModule + '/listAll', { params: params })