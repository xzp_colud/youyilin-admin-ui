import request from "@/utils/request"

const baseModule = '/goods.unit.data'

// 列表
export const unitDataPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getUnitData = params => request.get(baseModule + '/' + params)

// 新增
export const unitDataAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const unitDataEdit = data => request.post(baseModule + '/edit', data)

// 通过产品新增
export const unitDataAddByProduct = data => request.post(baseModule + '/addByProduct', data, { loading: false })

// 删除通过商品的随机ID
export const unitDataDelByProductId = data => request.post(baseModule + '/delByProductId', data, { loading: false })