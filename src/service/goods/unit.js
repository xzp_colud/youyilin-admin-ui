import request from "@/utils/request"

const baseModule = '/goods.unit'

// 列表
export const unitPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getUnit = params => request.get(baseModule + '/' + params)

// 新增
export const unitAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const unitEdit = data => request.post(baseModule + '/edit', data)

// 所有
export const unitAll = () => request.get(baseModule + '/listAll')