import request from "@/utils/request"

const baseModule = '/goods.space'

// 所有空间
export const spaceAll = () => request.get(baseModule + '/listAll')

// 新增
export const spaceAdd = data => request.post(baseModule + '/add', data)