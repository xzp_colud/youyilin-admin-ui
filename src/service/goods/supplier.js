import request from "@/utils/request"

const baseModule = '/goods.supplier'

// 列表
export const supplierPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getSupplier = params => request.get(baseModule + '/query/' + params)

// 所有
export const supplierAll = params => request.get(baseModule + '/listAll', { params: params })

// 新增
export const supplierAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const supplierEdit = data => request.post(baseModule + '/edit', data)