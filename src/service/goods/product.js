import request from "@/utils/request"

const baseModule = '/product'

// 列表
export const productPage = params => request.get(baseModule + '/getPageList', { params: params })

// 新增查询
export const getProductAdd = params => request.get(baseModule + '/queryAdd/' + params)

// 编辑查询
export const getProductEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 详情查询
export const getProductDetail = params => request.get(baseModule + '/queryDetail/' + params)

// 新增
export const productAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const productEdit = data => request.post(baseModule + '/edit', data)

// 删除
export const productDel = data => request.post(baseModule + '/del', data)

// 修改状态
export const updateProductStatus = data => request.post(baseModule + '/updateStatus', data)