import request from "@/utils/request"

const baseModule = '/dashboard'

// 客户数据
export const getDashboardCustomer = () => request.get(baseModule + '/customer')