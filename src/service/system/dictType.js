import request from "@/utils/request"

const baseModule = '/system.dict.type'

// 列表
export const pageDictType = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getDictTypeEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const addDictType = data => request.post(baseModule + '/add', data)

// 编辑
export const editDictType = data => request.post(baseModule + '/edit', data)