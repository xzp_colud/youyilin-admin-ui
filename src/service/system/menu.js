import request from "@/utils/request"

const baseModule = '/system.menu'

// 列表
export const pageMenu = params => request.get(baseModule + '/getPageTree', { params: params })

// 查询
export const getMenuAdd = () => request.get(baseModule + '/queryAdd')

// 查询
export const getMenuEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 下拉框 菜单
// export const menuSearchTree = () => request.get(baseModule + '/search/tree')

// 树型菜单
// export const menuTree = () => request.get(baseModule + '/tree')

// 用户 动态路由
export const getRouters = () => request.get(baseModule + '/getRouters')

// 新增
export const addMenu = data => request.post(baseModule + '/add', data)

// 编辑
export const editMenu = data => request.post(baseModule + '/edit', data)