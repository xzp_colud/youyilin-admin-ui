import request from "@/utils/request"

const baseModule = '/system.user'

// 列表
export const pageUser = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getUserAdd = () => request.get(baseModule + '/queryAdd')

// 查询
export const getUserEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const addUser = data => request.post(baseModule + '/add', data)

// 编辑
export const editUser = data => request.post(baseModule + '/edit', data)

// 重置密码
export const resetUserPwd = data => request.post(baseModule + '/resetPwd/' + data)