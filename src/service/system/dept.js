import request from "@/utils/request"

const baseModule = '/system.dept'

// 列表
export const pageDept = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getDeptAdd = () => request.get(baseModule + '/queryAdd')

// 查询
export const getDeptEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 树结构
// export const deptSearchTree = () => request.get(baseModule + '/queryTree')

// 新增
export const addDept = data => request.post(baseModule + '/add', data)

// 编辑
export const editDept = data => request.post(baseModule + '/edit', data)