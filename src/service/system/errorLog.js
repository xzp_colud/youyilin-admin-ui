import request from "@/utils/request"

const baseModule = '/system.error.log'

// 列表
export const pageErrorLog = params => request.get(baseModule + '/getPageList', { params: params })

// 删除
export const delErrorLog = data => request.post(baseModule + '/del', data)