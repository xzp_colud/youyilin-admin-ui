import request from "@/utils/request"

const baseModule = '/system.role'

// 列表
export const pageRole = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getRoleEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const addRole = data => request.post(baseModule + '/add', data)

// 编辑
export const editRole = data => request.post(baseModule + '/edit', data)