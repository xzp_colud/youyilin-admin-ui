import request from "@/utils/request"

const baseModule = '/system.dict.data'

// 列表
export const pageDictData = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getDictDataEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 根据字典类型查询
export const listDictDataByDictTypeList = data => request.post(baseModule + '/listByDictTypeList', data, { loading: false })

// 新增
export const addDictData = data => request.post(baseModule + '/add', data)

// 编辑
export const editDictData = data => request.post(baseModule + '/edit', data)

// 删除
export const delDictData = data => request.post(baseModule + '/del', data)