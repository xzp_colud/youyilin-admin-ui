import request from "@/utils/request"

const baseModule = '/system.notice'

// 列表
export const noticePage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getNotice = params => request.get(baseModule + '/query/' + params)

// 查询
export const getUpdaterdNotice = () => request.get(baseModule + '/getUpdated')

// 新增
export const noticeAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const noticeEdit = data => request.post(baseModule + '/edit', data)

// 禁用
export const noticeDisabled = data => request.post(baseModule + '/disabled', data)