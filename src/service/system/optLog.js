import request from "@/utils/request"

const baseModule = '/system.opt.log'

// 列表
export const pageOptLog = params => request.get(baseModule + '/getPageList', { params: params })

// 删除
export const delOptLog = data => request.post(baseModule + '/del', data)