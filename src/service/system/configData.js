import request from "@/utils/request"

const baseModule = '/system.config.data'

// 列表
export const configDataPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getConfigData = params => request.get(baseModule + '/query/' + params)

// 查询
export const listConfigDataByConfigId = params => request.get(baseModule + '/listByConfigId/' + params)

// 新增
export const configDataAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const configDataEdit = data => request.post(baseModule + '/edit', data)