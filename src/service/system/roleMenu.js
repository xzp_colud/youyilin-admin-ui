import request from "@/utils/request"

const baseModule = '/system.role.menu'

// 列表
export const getRoleAuth = params => request.get(baseModule + '/getAuth/' + params)

// 新增
export const authRoleMenu = data => request.post(baseModule + '/auth', data)