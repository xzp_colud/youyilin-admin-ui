import request from "@/utils/request"

const baseModule = '/system.ip'

// 列表
export const pageIp = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getIpEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const addIp = data => request.post(baseModule + '/add', data)

// 编辑
export const editIp = data => request.post(baseModule + '/edit', data)

// 删除
export const delIp = data => request.post(baseModule + '/del', data)