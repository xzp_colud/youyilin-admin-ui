import request from "@/utils/request"

const baseModule = '/system.file'

// 列表
export const filePage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getFile = params => request.get(baseModule + '/query/' + params)