import request from "@/utils/request"

const baseModule = '/system.notice.read'

// 列表 Table列表
export const noticeUserReadPage = params => request.get(baseModule + '/getPageList', { params: params })

// 列表 选择用户列表
export const noticeUserReadSendPage = params => request.get(baseModule + '/getSendPageList', { params: params })

// 列表 个人通知列表
export const noticeUserReadPageMe = params => request.get(baseModule + '/getUserPageList', { params: params })

// 未读数量
export const getNoReadNum = () => request.get(baseModule + '/getNoReadNum')

// 发送
export const noticeSend = data => request.post(baseModule + '/send', data)

// 撤销
export const noticeCancel = data => request.post(baseModule + '/cancel', data)

// 全部撤销
export const noticeCancelAll = data => request.post(baseModule + '/cancelAll', data)

// 单条已读
export const noticeRead = data => request.post(baseModule + '/read', data)

// 全部已读
export const noticeReadAll = () => request.post(baseModule + '/readAll')