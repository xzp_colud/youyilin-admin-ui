import request from "@/utils/request"

const baseModule = '/system.post'

// 列表
export const pagePost = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getPostAdd = () => request.get(baseModule + '/queryAdd')

// 查询
export const getPostEdit = params => request.get(baseModule + '/queryEdit/' + params)

// 新增
export const addPost = data => request.post(baseModule + '/add', data)

// 编辑
export const editPost = data => request.post(baseModule + '/edit', data)