import request from "@/utils/request"

const baseModule = '/address/'

// 列表
export const addressPage = params => request.get(baseModule + 'getPageList', { params: params })

// 查询
export const getAddressById = params => request.get(baseModule + params)

// 通过省份查询
export const getProvinceByName = params => request.get(baseModule + 'province/' + params)

// 下级地区
export const nextAddress = params => request.get(baseModule + 'next/' + params)

// 树结构
export const listTree = () => request.get(baseModule + 'listTree')

// 新增
export const addressAdd = data => request.post(baseModule + 'add', data)

// 更新
export const addressEdit = data => request.post(baseModule + 'edit', data)

// 删除
export const addressDel = data => request.post(baseModule + 'del')