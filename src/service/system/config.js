import request from "@/utils/request"

const baseModule = '/system.config'

// 列表
export const configPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getConfig = params => request.get(baseModule + '/query/' + params)

// 所有
export const listAllConfig = () => request.get(baseModule + '/listAll')

// 新增
export const configAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const configEdit = data => request.post(baseModule + '/edit', data)

// 更新配置
export const configUpdateData = data => request.post(baseModule + '/updateData', data)