import request from "@/utils/request"

const baseModule = '/system.login.log'

// 列表
export const pageLoginLog = params => request.get(baseModule + '/getPageList', { params: params })

// 删除
export const delLoginLog = data => request.post(baseModule + '/del', data)