import request from "@/utils/request"

const baseModule = '/warehouse'

// 列表
export const warehousePage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getWarehouse = params => request.get(baseModule + '/queryEdit/' + params)

// 所有
export const listAllWarehouse = () => request.get(baseModule + '/listAll')

// 新增
export const warehouseAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const warehouseEdit = data => request.post(baseModule + '/edit', data)

// 删除
export const warehouseDel = data => request.post(baseModule + '/del', data)