import request from "@/utils/request"

const baseModule = '/warehouse.area'

// 列表
export const warehouseAreaPage = params => request.get(baseModule + '/getPageList', { params: params })

// 查询
export const getWarehouseArea = params => request.get(baseModule + '/queryEdit/' + params)

// 所有库区
export const listAllArea = () => request.get(baseModule + '/listAll')

// 某仓库库区
export const listAllWarehouseArea = params => request.get(baseModule + '/listAllByWarehouseId/' + params)

// 新增
export const warehouseAreaAdd = data => request.post(baseModule + '/add', data)

// 编辑
export const warehouseAreaEdit = data => request.post(baseModule + '/edit', data)

// 删除
export const warehouseAreaDel = data => request.post(baseModule + '/del', data)