import request from "@/utils/request"

const baseModule = '/warehouse.inventory.lot'

// 列表
export const listInventoryLotPageByInventory = params => request.get(baseModule + '/listPageByInventory', { params: params })

// 批次信息删除
export const inventoryLotDel = data => request.post(baseModule + '/del', data)

// 盘点新增
export const inventoryLotSave = data => request.post(baseModule + '/save', data)

// 盘点出库
export const inventoryLotCheckOut = data => request.post(baseModule + '/checkOut', data)

// 盘点入库
export const inventoryLotCheckIn = data => request.post(baseModule + '/checkIn', data)

// 移动库区
export const inventoryLotMove = data => request.post(baseModule + '/move', data)