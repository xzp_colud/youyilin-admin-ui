import request from "@/utils/request"

const baseModule = '/warehouse.inventory'

// 列表
export const inventoryPage = params => request.get(baseModule + '/getPageList', { params: params })

// 异常列表
export const inventoryErrorList = () => request.get(baseModule + '/listError')

// 库存盘点 查询
export const listInventoryByProductName = params => request.get(baseModule + '/listByProductName', { params: params })

// 库存盘点 查询
export const listLikeInventoryByProductName = params => request.get(baseModule + '/listLikeByProductName', { params: params })

// 商品出库 查询
export const listInventoryOutBySkuId = params => request.get(baseModule + '/listInventoryOutBySkuId/' + params)

// 商品入库 查询
export const listInventoryInByProductId = params => request.get(baseModule + '/listInventoryInByProductId/' + params)

// 同步库存
export const asyncInventory = () => request.get(baseModule + '/asyncInventory')

// 库存信息删除
export const inventoryDel = data => request.post(baseModule + '/del', data)

// 移动库存信息
export const inventorykMove = data => request.post(baseModule + '/move', data)