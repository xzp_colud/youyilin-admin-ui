import request from "@/utils/request"

const baseModule = '/warehouse.inventory.log'

// 列表
export const inventoryLogPage = params => request.get(baseModule + '/getPageList', { params: params })

// 列表
export const listInventoryLogPage = params => request.get(baseModule + '/listPageByInventory', { params: params })

// 列表
export const listInventoryLotLogPage = params => request.get(baseModule + '/listPageByInventoryLot', { params: params })