import { createApp } from 'vue'
import Antd from 'ant-design-vue'
import App from './App.vue'
import router from './router/router'
import { createPinia } from 'pinia'

// css
import './config/css.config'
// 自定义指令
import config_directive from './config/directive.config'
// 自定义组件
import config_components from './config/components.config'
// 自定义工具类
import config_util from './config/util.config'

const pinia = createPinia()
const app = createApp(App)

config_directive(app)
config_components(app)
config_util(app)

app.use(Antd)
app.use(pinia)
app.use(router)

app.mount('#app')

export default app