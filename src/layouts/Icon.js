import { createVNode } from 'vue'
import * as $Icon from './IconExample'

export const Icon = (props) => {
    const { icon } = props
    var antIcon = $Icon
    return createVNode(antIcon[icon])
}